<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TM470</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
        <link rel="stylesheet" href="/css/custom.css">
    </head>
    <body>
    <div class="navbar navbar-dark bg-dark">
        <div class="container d-flex justify-content-between">
            <a href="/" class="navbar-brand">TM470 – IT Project - B8329896</a>
            <a class="navbar-brand" target="_blank" href="https://tm470.softquantum.io/">Blog (require login)</a>
            <a class="navbar-brand" target="_blank" href="https://bitbucket.org/maxd23/api-tm470">Bitbucket Repository</a>

        </div>
    </div>

    <section class="jumbotron">
        <div class="container">
            <p class="lead text-muted alert alert-primary">Refresh the page to get a new image.  Unsplash API is limiting 50 requests per hour.</p>
            <p class="text-muted">This page exists to test the project's Web API (microservice) created rendering images with Blender 3D as its core engine.  It is tested with images URL from unsplash.com, using their API, and sent in an HTTP POST Json request to the microservice.</p>
        </div>
    </section>
        <div class="album">
            <div class="container">
                <div class="row">
                    <div class="container-fluid">
                        <div class="card">
                            <img class="card-img-top" src="{{ $url }}">
                            <div class="card-body">
                                <h4 class="card-title">Source</h4>
                                <p class="card-text">Image source from Unsplash.com: <br> {{ $url }}.</p>
                            </div>
                        </div>
                        <div class="card" style="width: 60%">
                            <img class="card-img-top" src="{{ asset('storage' . $render) }}">
                            <div class="card-body">
                                <h4 class="card-title">Rendered</h4>
                                <p class="card-text">By the time this page has been loaded, the Web API treated the image source. The output is an image rendered by Blender showing the source image applied on a defined part of this cube.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <footer class="text-muted">
        <div class="container">
            <p class="float-right">
                <a href="#">Back to top</a>
            </p>
            <p>Pictures are the property of their respective authors &copy; <a target="_blank" href="https://unsplash.com">Unsplash.com</a></p><p>Blender is a registered trademark® of the Blender Foundation &copy; <a target="_blank" href="https://www.blender.org">Blender.</a></p>

        </div>
    </footer>
    </body>
</html>
