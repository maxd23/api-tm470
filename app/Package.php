<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static find(int $int)
 */
class Package extends Model
{
    protected $guarded = [];
}

