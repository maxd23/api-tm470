<?php

namespace App;

use GuzzleHttp\Client;

class UnsplashImage
{
    public function get() {
        $client = new Client(['base_uri' => 'https://api.unsplash.com/']);
        $response = $client->request('GET', 'photos/random/?client_id=6b75d2342634a914cf1741cc327daab5fd8505f4974502787d94f796ff28951d');
        $body = $response->getBody()->getContents();
        return array_column((array) json_decode($body), 'small')[0];
    }
}