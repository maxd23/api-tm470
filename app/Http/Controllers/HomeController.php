<?php

namespace App\Http\Controllers;

use App\Package;
use App\Blender;
use GuzzleHttp\Client;
use App\UnsplashImage;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    /**
     * @return Factory|View|Application
     * @throws GuzzleException
     */
    public function index(): Factory|View|Application
    {
        $image = "";

        $url = (new UnsplashImage())->get();
        if($url == ''){
            $url = 'https://images.unsplash.com/photo-1495329144860-da404d597791?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&s=15c2080eba7d746107530dc34f0c2f08';
        }
        $package = Package::find(1);
        $client = new Client();
        $response = $client->request('POST', 'https://tm470-api.softquantum.io/api/packages/' . $package->id, [
            'json' => ['url' => $url],
            'headers' => ['Content-Type' => 'application/json']
        ]);
//        $response = $client->request('POST', 'http://api-tm470.test/api/packages/' . $package->id, [
//            'json' => ['url' => $url],
//            'headers' => ['Content-Type' => 'application/json']
//        ]);
        $image_path = $response->getBody()->getContents();
        $blender = new Blender();
        $randomLetters = Str::random(3);
        $blender->render($image_path, $package->id . '_' .$randomLetters);
        $render = '/rendered/render_'.$package->id.'_'.$randomLetters.'_0000.png';

        return view('test', compact( 'url', 'package', 'render'));
    }
}
