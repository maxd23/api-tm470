<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;


class ImagesController extends Controller
{

    public function create(Request $request)
    {
            $filename = Str::random(12) . '_brand.jpg';
            $file = file_get_contents($request->get("url", "public/brands/notsaved.jpg"));
            Storage::disk('local')
                ->put(
                    'public/brands/' .$filename,
                    $file
                );

            return 'public/brands/'. $filename;
    }
}
