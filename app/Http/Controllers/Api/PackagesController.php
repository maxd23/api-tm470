<?php

namespace App\Http\Controllers\Api;

use App\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PackagesController extends Controller
{
    public function index()
    {
       //
    }

    public function show(Package $package)
    {
        return $package;
    }
}
