<?php

namespace App;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class Blender
{
    public function __construct()
    {
        //
    }

    /**
     * @param $imagePath
     * @param $brand
     * @return string
     *
     * blender -b package.blend -P myscript.py -o //render_<brand_name>_ -F PNG -x 1 -f 0 -- <image_path>
     */
    public function render($imagePath, $brand): string
    {
        // Use this one on macOS as it seems to have issues to launch Blender even if your path is correctly set.
//         $process = new Process([
//             '/Applications/Blender.app/Contents/MacOS/blender',
//             '-b',
//             'package.blend',
//             '-P',
//             'myscript.py',
//             '-o',
//             '//public/rendered/render_' . $brand . '_',
//             '-F',
//             'PNG',
//             '-x',
//             1,
//             '-f',
//             0,
//             '-noaudio',
//             '--',
//             'cycles-device',
//             'CPU',
//             $imagePath
//            ],
//             storage_path('app/')
//         );

         // Tested in Ubuntu 22.10 with Blender 3.4.1
        $process = new Process([
            'blender',
            '-b',
            'package.blend',
            '-P',
            'myscript.py',
            '-o',
            '//public/rendered/render_' . $brand . '_',
            '-F',
            'PNG',
            '-x',
            1,
            '-f',
            0,
            '-noaudio',
            '--',
            $imagePath
        ],
            storage_path('app/')
        );

        // set the timeout to 3 minutes...
        $process->setTimeout(180);

        //launch the command where the python script is
        $process->run();


        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        return $process->getOutput();
    }
}