# Run as: 
# blender -b package.blend -P myscript.py -o //render_<brandName> -F PNG -x 1 -f 0 -- <texture_brands.jpg>
import bpy, sys, os, bmesh

# Assume the last argument is image path
imagePath = sys.argv[-1]

if os.path.exists(imagePath):
    # Cube
    myPackage = bpy.context.active_object.material_slots[0].material

    # load the image
    img = bpy.data.images.load(imagePath)

    # config the object
    myPackage.use_nodes=True

    #setup the node_tree and links as you would manually on shader Editor
    #to define an image texture for a material

    material_output = myPackage.node_tree.nodes.get('Material Output')
    principled_BSDF = myPackage.node_tree.nodes.get('Principled BSDF')

    tex_node = myPackage.node_tree.nodes.new('ShaderNodeTexImage')
    tex_node.image = img

    myPackage.node_tree.links.new(tex_node.outputs[0], principled_BSDF.inputs[0])

    # grab mesh data for the active mesh.
    bpy.ops.object.mode_set(mode="EDIT")
    # mesh = bpy.context.active_object.data
    # bm = bmesh.from_edit_mesh(bpy.context.object.data)
    # bm.faces.ensure_lookup_table()
    bpy.ops.uv.unwrap()
    # uv_layer = bm.loops.layers.uv.active

    # assign the uv values for the vertices of each face. This example
    # assumes the default cube
    # for i in range(6):

       # loop_data = bm.faces[i].loops
       # uv_data = loop_data[0][uv_layer].uv
       # uv_data.x = 1.0
       # uv_data.y = 0.0

       # uv_data = loop_data[1][uv_layer].uv
       # uv_data.x = 1.0
       # uv_data.y = 1.0

       # uv_data = loop_data[2][uv_layer].uv
       # uv_data.x = 0.0
       # uv_data.y = 1.0

       # uv_data = loop_data[3][uv_layer].uv
       # uv_data.x = 0.0
       # uv_data.y = 0.0

    bpy.ops.object.mode_set(mode="OBJECT")
else:
    print("Missing Image:", imagePath)


