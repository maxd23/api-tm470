<?php

namespace Tests\Feature;

use App\Package;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ViewPackageTest extends TestCase
{
    use RefreshDatabase;

    /**  @test */
    public function user_can_view_a_package()
    {
        //Arrange
        $package = factory(Package::class)->create([
            'title' => 'The magical cylinder'
        ]);
        //Act
        $this->withoutExceptionHandling();
        $response = $this->get('/api/packages/' . $package->id);
        //Assert
        $response->assertStatus(200);
        $response->assertJson([
            'title' => 'The magical cylinder',
        ]);

    }
}
