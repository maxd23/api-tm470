<?php

namespace Tests\Feature;

use App\UnsplashImage;
use App\Blender;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SendImageToBlenderTest extends TestCase
{
    use RefreshDatabase;

    protected $url, $package, $image;

    protected function setUp()
    {
        parent::setUp();
        $this->url = (new UnsplashImage())->get();
        $this->package = factory(\App\Package::class)->create();
        $this->image = '';

    }

    /** @test */
    public function it_can_upload_an_image_to_a_package()
    {
        //Arrange
        // disable exception handling from Laravel to have complete error messages
        $this->withoutExceptionHandling();

        //Act
        $response = $this->json('POST','/api/packages/' . $this->package->id . '/', [ 'url' => $this->url ]);
        $this->image = $response->getContent();
        $blender = new Blender();
        $blender->render($this->image, $this->package->id);

        //Assert
        $response->assertStatus(200);
        $response->assertSee('.jpg');

    }
}
