<?php

namespace Tests\Feature;

use App\Package;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ViewPackageListingTest extends TestCase
{
    use RefreshDatabase;
    /**  @test */
    public function user_can_view_a_package_listing()
    {
        $packages = new Collection();
        //Arrange
        $packages->add(factory(\App\Package::class)->create([
            'title' => 'The cube'
        ]));

        $packages->add(factory(\App\Package::class)->create([
            'title' => 'The cylinder'
        ]));

        //Act
        $this->withoutExceptionHandling();
        $response = $this->get('/api/packages');
        $response_plucked = json_encode(array_values(array_pluck($response->getOriginalContent(), 'title')));

        //Assert
        $response->assertStatus(200);
        $this->assertEquals($packages->pluck('title')->toJson(), $response_plucked);



    }
}
