<?php

namespace Tests\Unit;

use App\Blender;
use Tests\TestCase;

class CanCreateBlenderImageTest extends TestCase
{
    /** @test */
    public function it_can_create_a_blender() {
        $blender = new Blender();
        $this->assertInstanceOf(Blender::class, $blender);
    }

}