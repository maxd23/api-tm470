<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\UnsplashImage;


class GetImageFromUnsplashTest extends TestCase
{
    private $url;

    public function setUp() {
        parent::setUp();
        $this->url = (new UnsplashImage())->get();
    }

    /** @test */
    public function it_can_receive_an_image_from_unsplash()
    {
        //Is it a string starting with https?
        $this->assertStringStartsWith('https', $this->url);

    }

}