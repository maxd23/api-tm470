<?php

use Faker\Generator as Faker;

$factory->define(App\Package::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'capacity' => $faker->numberBetween(50, 1000),
        'model' => 'package.blend'
    ];
});